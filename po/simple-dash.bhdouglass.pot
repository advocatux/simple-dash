# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the simple-dash.bhdouglass package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: simple-dash.bhdouglass\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-28 03:49+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/AppsPage.qml:28
msgid "Apps"
msgstr ""

#: ../qml/AppsPage.qml:32 ../qml/AboutPage.qml:7
msgid "About"
msgstr ""

#: ../qml/AppsPage.qml:39 ../qml/SettingsPage.qml:14
msgid "Settings"
msgstr ""

#: ../qml/AppsPage.qml:109
msgid "Unlock settings"
msgstr ""

#: ../qml/AppsPage.qml:133 ../qml/SettingsPage.qml:39
msgid "Unlock"
msgstr ""

#: ../qml/AppsPage.qml:139 ../qml/SettingsPage.qml:192
msgid "Cancel"
msgstr ""

#: ../qml/AboutPage.qml:50 simple-dash.desktop.in.h:1
msgid "Simple Dash"
msgstr ""

#: ../qml/AboutPage.qml:57
msgid ""
"A Brian Douglass app, consider donating if you like it and want to see more "
"apps like it!"
msgstr ""

#: ../qml/AboutPage.qml:65
msgid "Donate"
msgstr ""

#: ../qml/SettingsPage.qml:34
msgid "Lock Settings"
msgstr ""

#: ../qml/SettingsPage.qml:39 ../qml/SettingsPage.qml:186
msgid "Lock"
msgstr ""

#: ../qml/SettingsPage.qml:52
msgid "Autostart"
msgstr ""

#: ../qml/SettingsPage.qml:57
msgid "Uninstall"
msgstr ""

#: ../qml/SettingsPage.qml:57
msgid "Install"
msgstr ""

#: ../qml/SettingsPage.qml:70
msgid "*You will need to restart after installing or uninstalling"
msgstr ""

#: ../qml/SettingsPage.qml:74
msgid "Visible Apps"
msgstr ""

#: ../qml/SettingsPage.qml:148
msgid "Lock settings"
msgstr ""

#: ../qml/SettingsPage.qml:174
msgid "Confirm password"
msgstr ""
