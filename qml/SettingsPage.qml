import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import SimpleDash 1.0
import "Components"

Page {
    id: settingsPage

    header: PageHeader {
        id: header
        title: i18n.tr('Settings')
    }

    Component.onCompleted: Applications.refresh()

    ColumnLayout {
        id: settingsColumn

        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            rightMargin: units.gu(1)
            leftMargin: units.gu(1)
            topMargin: units.gu(2)
        }

        spacing: units.gu(2)

        Label {
            text: i18n.tr('Lock Settings')
            textSize: Label.Large
        }

        Button {
            text: settings.password ? i18n.tr('Unlock') : i18n.tr('Lock')
            color: settings.password ? UbuntuColors.red : UbuntuColors.green
            onClicked: {
                if (settings.password) {
                    settings.password = '';
                }
                else {
                    PopupUtils.open(setPassword, settingsPage);
                }
            }
        }

        Label {
            text: i18n.tr('Autostart')
            textSize: Label.Large
        }

        Button {
            text: Applications.isDashSet ? i18n.tr('Uninstall') : i18n.tr('Install')
            color: Applications.isDashSet ? UbuntuColors.red : UbuntuColors.green
            onClicked: {
                if (Applications.isDashSet) {
                    Applications.unsetDash();
                }
                else {
                    Applications.setDash();
                }
            }
        }

        Label {
            text: i18n.tr('*You will need to restart after installing or uninstalling')
        }

        Label {
            text: i18n.tr('Visible Apps')
            textSize: Label.Large
        }
    }

    Flickable {
        anchors {
            top: settingsColumn.bottom
            right: parent.right
            left: parent.left
            bottom: parent.bottom
        }

        clip: true
        contentHeight: grid.height + units.gu(5)

        GridLayout {
            id: grid

            anchors {
                top: parent.top
                right: parent.right
                left: parent.left
                margins: units.gu(1)
            }

            columns: 4
            columnSpacing: units.gu(1)
            rowSpacing: units.gu(1)

            Repeater {
                model: Applications.list

                delegate: Launcher {
                    Layout.fillWidth: true
                    Layout.preferredHeight: width + units.gu(2)
                    Layout.maximumWidth: appWidth

                    app: model
                    color: 'black'

                    onClicked: {
                        if (model.selected) {
                            Applications.removeApp(model.id);
                        }
                        else {
                            Applications.selectApp(model.id);
                        }
                    }
                    onPressAndHold: Qt.openUrlExternally(model.uri)

                    Icon {
                        visible: model.selected
                        name: 'tick'
                        color: UbuntuColors.green
                        anchors {
                            top: parent.top
                            right: parent.right
                        }
                        width: units.gu(2)
                        height: width
                    }
                }
            }
        }
    }

    Component {
        id: setPassword

        Dialog {
            id: setPasswordDialog

            Label {
                text: i18n.tr('Lock settings')
                horizontalAlignment: Label.AlignHCenter
                textSize: Label.Large
            }

            function checkPassword() {
                if (password.text == passwordConfirm.text) {
                    password.focus = false;
                    passwordConfirm.focus = false;

                    settings.password = password.text;
                    PopupUtils.close(setPasswordDialog);
                }
                else {
                    // TODO error message
                }
            }

            TextField {
                id: password

                echoMode: TextInput.Password
                onAccepted: checkPassword()
            }

            Label {
                text: i18n.tr('Confirm password');
                horizontalAlignment: Label.AlignHCenter
            }

            TextField {
                id: passwordConfirm

                echoMode: TextInput.Password
                onAccepted: checkPassword()
            }

            Button {
                text: i18n.tr('Lock')
                color: UbuntuColors.green
                onClicked: checkPassword()
            }

            Button {
                text: i18n.tr('Cancel')
                onClicked: PopupUtils.close(setPasswordDialog)
            }
        }
    }
}
